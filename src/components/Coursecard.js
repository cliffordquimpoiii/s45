import React, {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import PropTypes from 'prop-types';
export default function Coursecard ({courseProp}){
	//check to see if the data was succesfully passed
	// console.log(props)
	// console.log(typeof props)
	const  {name, description, price} = courseProp;

	//Let's create our useState hooks to store its state
	//States are used to keep track of information related to individual componenets
	/*Syntax:
		const [currentValue(getter), updatedValue(setter)] = useState(InitialGetterValue)
	*/
	const [count, setCount] = useState(0);
	const [seatCount, setSeatCount] = useState(30)
	//statehook that inidcates availability of course for enrollment (enroll)
	const [isOpen, setIsOpen] = useState(true)
	console.log(count);

	const enroll = () =>{
				setCount(count +1);
				console.log("Enrolees: " + count)
				setSeatCount(seatCount -1)
				console.log("Seat: " + seatCount)
			
		// console.log("Enrolees " + count)
	}
	//when you call useEffect, you're telling React to run your "effect" function after flsuhing changes to the DOM. Effects are decalred inside the component so they have access to it's props and states
	useEffect(()=>{
		if(seatCount === 0){
			setIsOpen(false)
		}
	},[seatCount])

	return(
		<Row className ="rowCard">
			<Col>
				<Card className = "courseCard p-3">
					<Card.Body>
						<Card.Title>
							<h2>{name}</h2>
						</Card.Title>
						<Card.Text className = "my-3">
							<h4>Description:</h4>
								{description}
						</Card.Text>
						<Card.Text>
							<h4>Price:</h4>
							Php {price}
						</Card.Text>
						<Card.Text>
							Enrolees: {count}
						</Card.Text>
						<Card.Text>
							Seats: {seatCount}
						</Card.Text>
						<Card.Text>
							{isOpen ?
							<Button variant ="primary" onClick={enroll}> Enroll </Button>
							:
							<Button variant ="primary" disabled> Enroll </Button>
						

							}
						</Card.Text>

					</Card.Body>
						
				</Card>

			</Col>
				
			
		</Row>

		)
}

//Check if the coursecard component is getting the correct prop types.
//Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is from one component to the next.

Coursecard.propTypes = {
	//shape() method it is used to check if a prop object conforms to a specific shape.
	courseProp: PropTypes.shape({
		//Define the properties and their expected types.
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}