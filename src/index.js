import React from 'react';
import ReactDOM from 'react-dom/client';
import 'bootstrap/dist/css/bootstrap.min.css';
import App from './App';


//ReactJS doe snot like rendering two adjacent elements, Instead the adjacent elements must be wrapped by a parent element or React Fragments (<> </>)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <App />
 
);

// JSX it is a syntax used in Reactjs
//Javascript + XML, It is an extension of Js that let's us create objects which will then be compiled and added as HTML elements

//With JSX, we are able to create HTML
