import React, {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';
export default function Register(){
	//State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password1, setpassword1] = useState('');
	const [password2, setpassword2] = useState('');
	//State to determine whetehr submit button is enable or not in rendering
	const [isActive, setIsActive] = useState(true)

	useEffect(() =>{
		//Validation to Enavle submit button when all fields are populated and passwords match
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2)){
			setIsActive(true);
		} 
		else
		{
			setIsActive(false);
		}
	}, [email, password1, password2])

	function registerUser(e){
		//Prevents page redirection via form submission
		e.preventDefault();
		//Clear Input fields
		setEmail('');
		setpassword1('');
		setpassword2('');

		Swal.fire({
			title: "Yay!",
			icon: "success",
			text: "Thank you for registering"
		});
	}
	return (
		<Form onSubmit={(e)=> registerUser(e)}>
			<h1>Register</h1>
			<Form.Group>
				<Form.Label>Email Address</Form.Label>
				<Form.Control type="email" 
				placeholder="Enter Email" 
				required 
				value={email}
				onChange={e => setEmail(e.target.value)} 
				/>
				<Form.Text className="text-muted">
					'Well never share your email with anyone else'
				</Form.Text>
			</Form.Group>
			<Form.Group>
				<Form.Label>Password</Form.Label>
				<Form.Control 
				type="password" 
				placeholder="Enter Password" 
				required
				value={password1}
				onChange={e => setpassword1(e.target.value)} 
				/>
				
			</Form.Group>

			<Form.Group>
				<Form.Label> Confirm Password</Form.Label>
				<Form.Control 
				type="password" 
				placeholder="Confirm Password" 
				required
				value={password2} 
				onChange={e => setpassword2(e.target.value)}
				/>
			</Form.Group>
			{isActive ?
				<Button variant="primary" type="submit" className="mt-3">Submit</Button>
				:
				<Button variant="primary" type="submit" className="mt-3" disabled>Submit</Button>

			}
			
		</Form>
		)
}