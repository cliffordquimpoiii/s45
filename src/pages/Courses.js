import React from 'react';
import coursesData from '../mockData/coursesData';
import Coursecard from '../components/Coursecard';




export default function Courses() {
	//Check to see if the mockc data was captured
	console.log(coursesData[0]);


	// for us to be able to display all the courses from the data file, we are going to use map ()
// The "map" method loops through all the individual course objects in our array and returns a component for each course.

//Multiple componens created through the map methods must have a UNIQuE KEY that will help React JS identify which componnets/elements have been changed, added or removed.
	const courses = coursesData.map(course => {
		return(
			<Coursecard courseProp={course} />
			)
	}) 

	
	return (
	<>
		<h1> Courses </h1>
		{courses}
	</>
		)
}